import random

class DecisionTree:

    def __init__(self, input_len, depth, input_domain=float, depth_variance=0, range=[True, False]):

        # Verify input integrity

        # Random depth stuff
        if depth_variance == 0:
            self.depth = depth
        else:
            self.depth = int(random.gauss(depth, depth_variance))

        # Set up fields
        self.input_len = input_len
        self.input_domain = input_domain
        self.range = range

        self.tree = [None] * (len(self.range) ** self.depth)

    def build(self, inputs):

        # Verify input integrity
        valid = True
        for input in inputs:

            if not isinstance(input, tuple) or len(input[0]) != self.input_len:
                valid = False
                break

            (data, result) = input

            if not instanceof(data, list) or len(data) != self.input_len:
                valid = False
                break

            if result not in self.range:
                valid = False
                break

            # TODO: validate input type?

        if not valid:
            raise Exception("Invalid input")
        else:
            pass

    def decide(self, data):

        # Run through tree, return result
        pass
